# ICANN 2020 Paper submission

Code repository for "A Neural Framework for Learning Invariant Physical Relations in Multimodal Sensory Processing" by Du Xiaorui, Yavuzhan Erdem, Immanuel Schweizer and Cristian Axenie submitted at ENNS ICANN2020 (15th – 18th September 2020).

## Abstract 

Humans learn to detect perceptual information for guiding action adaptively as their sensory and motor systems are developing. This process enables humans to recognize and represent stimuli invariant to various transformations and build a consistent representation of the self and physical world. Such representations preserve the invariant physical relations among the multiple perceived sensory cues. 
This work is an attempt to exploit these principles in an engineered system. We instantiate a neural network architecture capable of learning, in an unsupervised manner, relations among multiple sensory cues. The system combines computational principles, such as competition, cooperation, and correlation, in a neurally plausible computational substrate. It achieves that through a parallel and distributed processing architecture in which the relations among the multiple sensory quantities are extracted from time-sequenced data.
We describe the core system functionality when learning arbitrary non-linear relations in low-dimensional sensory data. Here, an initial benefit rises from the fact that such a network can be engineered in a relatively straightforward way without prior information about the sensors and their interactions. Moreover, alleviating the need for tedious modeling and parameterization, the network converges to a consistent description of any arbitrary high-dimensional multisensory setup. We demonstrate this through a real-world learning problem, where, from standard RGB camera frames, the network learns the relations between physical quantities such as light intensity, spatial gradient, and optical flow, describing a visual scene.

## Basic idea
Using cortical maps as neural substrate for distributed representations of sensory streams, our system is able to learn its connectivity (i.e., structure) from the long-term evolution of sensory observations. This process mimics a typical development process where self-construction (connectivity learning), self-organization, and correlation extraction ensure a refined and stable representation and processing substrate. Following these principles, we propose a model based on Self-Organizing Maps (SOM) and Hebbian Learning (HL) as main ingredients for extracting underlying correlations in sensory data, the basis for subsequently extracting invariant representations.

## Quick Start

### 1- How to run 1D experiment.

- cd to the 1D_experiment folder.

- run python3 main.py.

- click the Visua_on button.

- click the start button.


<div align=center><img width="320" height="240" src="https://gitlab.com/akii-microlab/icann20/-/raw/master/images/1.png"/></div>

**Note:**

if you want speed up the training process, you can click the Visua_off button.



### 2- How to run image process.

- Download the whole files from the link:

https://drive.google.com/open?id=1I6a21i7N86tNrdttCgA19UEJR9vV_SUQ

- Test step:

	1.Set train = 0 in config_FGV.txt(config_IG.txt)

	2.Run
	    
	        python3 trainFGV_dense.py


Distribution of DxDyV

<div align=center><img width="320" height="240" src="https://gitlab.com/akii-microlab/icann20/-/raw/master/images/Distribution_of_DxDyV.jpg"/></div>

Distribution of FxFy

<div align=center><img width="320" height="240" src="https://gitlab.com/akii-microlab/icann20/-/raw/master/images/Distribution_of_FxFy.jpg"/></div>

HL matrix of FxFy & GV

<div align=center><img width="320" height="240" src="https://gitlab.com/akii-microlab/icann20/-/raw/master/images/HL_matrix_FxFy_GV.jpg"/></div>

Final image.

<div align=center><img width="250" height="250" src="https://gitlab.com/akii-microlab/icann20/-/raw/master/images/Optical_Flow.jpg"/></div>

Expected optical flow image

<div align=center><img width="250" height="250" src="https://gitlab.com/akii-microlab/icann20/-/raw/master/images/expected_flow.jpg"/></div>

Reconstructed optical flow image

<div align=center><img width="250" height="250" src="https://gitlab.com/akii-microlab/icann20/-/raw/master/images/reconstructed_flow.jpg"/></div>



	        python3 trainIG.py

	
Reconstructed Dx image

<div align=center><img width="250" height="250" src="https://gitlab.com/akii-microlab/icann20/-/raw/master/images/rebuild_picture_Dx.jpg"/></div>

Expected Dx image	

<div align=center><img width="320" height="240" src="https://gitlab.com/akii-microlab/icann20/-/raw/master/images/2.jpg"/></div>

HL matrix of IG

<div align=center><img width="320" height="240" src="https://gitlab.com/akii-microlab/icann20/-/raw/master/images/HL_matrix_IG.jpg"/></div>

**Note:**

	After runing, you will get results in test_results folder



- Train step:

	1.Set train = 1 in config_FGV.txt(config_IG.txt)

	2.Run

	python3 trainFGV_dense.py
	python3 trainIG.py

**Note:**

	After runing, you will get .pkl files in the current folder.